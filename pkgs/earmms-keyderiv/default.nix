{ lib
, rustPlatform
, fetchFromGitLab
, pkg-config
, openssl
, libsodium
}:

rustPlatform.buildRustPackage rec {
  pname = "earmms-keyderiv";
  version = "2.0.0";

  src = fetchFromGitLab {
    owner = "againstprisons";
    repo = "earmms_keyderiv";
    rev = "v${version}";
    sha256 = "0s7nv34ar4rwfnyyqsac4m09sqcz8vf7kzaydqxjzqc1cbzphg2g";
  };

  cargoDepsName = pname;
  cargoSha256 = "1mg9ylavy585zynrxx6153n0pfpbdwq3zgk0j9bd6miibhj6zq4y";

  nativeBuildInputs = [ pkg-config ];
  buildInputs = [
    openssl
    libsodium
  ];

  RUST_SODIUM_USE_PKG_CONFIG = 1;

  meta = with lib; {
    description = "Key derivation microservice for EARMMS and other PAPA projects";
    homepage = "https://gitlab.com/againstprisons/earmms_keyderiv";
    platforms = platforms.all;
    license = licenses.mit;
    maintainers = [ ];
  };
}
