{ stdenv
, lib
, callPackage
, fetchFromGitLab
, fetchurl
, fetchgit
, nix-gitignore
, bundlerEnv
, ruby
, rsync
, pkg-config
, curl
, zlib
, libsodium
, libxml2
, libxslt
, postgresql
, shared-mime-info
, makeWrapper
}@args:

with lib;

let
  pname = "earmms";
  version = "2.3.1";

  src = fetchFromGitLab {
    owner = "againstprisons";
    repo = pname;
    rev = "v${version}";
    sha256 = "18r5rf8nd4b7i8sw2jndk3jmqxkpl26k46svzfi14ybc10qy5568";
  };

  gemDependencies = bundlerEnv {
    inherit ruby;
    name = "${pname}-${version}-gemset";
    gemdir = src;
  };

  nodeDependencies = 
    let
      nodePkgs = import (src + "/node-packages.nix") {
        inherit stdenv lib fetchurl fetchgit nix-gitignore;
        nodeEnv = callPackage (<nixpkgs> + "/pkgs/development/node-packages/node-env.nix") { };
      };
    in nodePkgs.nodeDependencies;

  libraryPath = lib.makeLibraryPath libraryDependencies;
  libraryDependencies = [
    curl
    zlib
    libsodium
    libxml2
    libxslt
    postgresql
  ];

in stdenv.mkDerivation {
  name = "${pname}-${version}";
  inherit src;

  inherit ruby gemDependencies;
  inherit nodeDependencies;
  inherit libraryDependencies libraryPath;

  nativeBuildInputs = [ pkg-config makeWrapper ];
  buildInputs = [
    ruby
    gemDependencies
    nodeDependencies
    libraryDependencies
  ];

  gemBinsToWrap = [ "puma" "rake" "sequel" ];

  installPhase = ''
    mkdir $out

    ### Link libraries into place for easy access
    mkdir $out/lib
    ln -s ${nodeDependencies}/lib/node_modules $out/lib/node_modules
    ln -s ${gemDependencies}/lib/ruby $out/lib/ruby

    ### Make Bundler happy
    mkdir -p $out/bundle/ruby/${ruby.version.libDir}
    for part in ''$(find $out/lib/ruby/gems/${ruby.version.libDir} -maxdepth 1 -type d -exec basename {} \;)
    do
      ln -s \
        $out/lib/ruby/gems/${ruby.version.libDir}/$part \
        $out/bundle/ruby/${ruby.version.libDir}/$part
    done

    ### Copy in the EARMMS code
    mkdir $out/earmms
    ${rsync}/bin/rsync -a --exclude vendor --exclude public $src/* $out/earmms/
    for xdir in public vendor
    do
      mkdir $out/earmms/$xdir
      ${rsync}/bin/rsync -a $src/$xdir/* $out/earmms/$xdir/
    done

    ### Link in node_modules
    ln -s $out/lib/node_modules $out/earmms/node_modules

    ### Run webpack
    cd $out/earmms
    ${nodeDependencies}/bin/webpack
    cd $out

    ### Make wrapper shell scripts
    mkdir $out/bin
    makeWrapper ${ruby}/bin/bundle $out/bin/earmms-bundle \
      --argv0 bundle \
      --unset BUNDLE_ORIG_PATH \
      --unset BUNDLE_ORIG_GEMFILE \
      --set BUNDLE_IGNORE_CONFIG 1 \
      --set BUNDLE_FROZEN 1 \
      --set BUNDLE_GEMFILE $out/earmms/Gemfile \
      --set BUNDLE_PATH $out/bundle \
      --set GEM_PATH $out/${ruby.gemPath} \
      --set GEM_HOME $out/${ruby.gemPath} \
      --set LD_LIBRARY_PATH ${lib.escapeShellArg libraryPath} \
      --run "cd $out/earmms"
    for bin in $gemBinsToWrap
    do
      makeWrapper $out/bin/earmms-bundle $out/bin/earmms-$bin \
        --argv0 earmms-$bin \
        --add-flags exec \
        --add-flags $bin
    done
  '';

  meta = with lib; {
    description = "EARMMS - Encrypted At Rest Membership Management System";
    homepage = "https://gitlab.com/againstprisons/earmms";
    platforms = ruby.meta.platforms;
    license = licenses.agpl3Plus;
    maintainers = [ ];
  };
}
