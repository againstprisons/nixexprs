{ config
, options
, pkgs
, lib
, ...
}:

with lib;

let
  papalib = (import ../../lib { }).papa;
  inherit (papalib.types) isKeyderivKey keyderivKey;

  pkgDesc = "Key derivation microservice for EARMMS and other PAPA projects";
  cfg = config.services.earmms-keyderiv;
  opt = options.services.earmms-keyderiv;

  targetOpts = { name, ... }: {
    options = {
      name = mkOption {
        type = types.str;
        description = "Target name (defaults to the entry name)";
        default = name;
      };

      sharedSecret = mkOption {
        type = keyderivKey;
        description = "Shared secret used for in-transit encryption";
      };

      indexKey = mkOption {
        type = keyderivKey;
        description = "Key used for indexing";
      };

      encryptKey = mkOption {
        type = keyderivKey;
        description = "Key used for encryption";
      };
    };
  };

  keyderivConfig =
    let
      toml = pkgs.formats.toml {};
      mkConfig = targets: {
        target = lib.mapAttrsToList (_: v: {
          inherit (v) name;
          shared_secret = v.sharedSecret;
          index_key = v.indexKey;
          encrypt_key = v.encryptKey;
        }) targets;
      };
    in toml.generate "earmms-keyderiv-config.toml" (mkConfig cfg.targets);

in
{
  options = {
    services.earmms-keyderiv = {
      enable = mkEnableOption "earmms-keyderiv";

      package = mkOption {
        type = types.package;
        default = pkgs.earmms-keyderiv;
        defaultText = "pkgs.earmms-keyderiv";
      };

      user = mkOption {
        type = types.str;
        default = "earmms-keyderiv";
      };

      group = mkOption {
        type = types.str;
        default = "earmms-keyderiv";
      };

      listenHost = mkOption {
        type = types.str;
        default = "[::1]";
      };

      listenPort = mkOption {
        type = types.port;
        default = 23198;
      };

      targets = mkOption {
        description = "Attribute set of keyderiv targets to add to the configuration";
        type = types.attrsOf (types.submodule targetOpts);
        default = {};

        example = literalExample ''
          {
            development = {
              sharedSecret = "0000000000000000000000000000000000000000000000000000000000000000";
              indexKey = "0000000000000000000000000000000000000000000000000000000000000000";
              encryptKey = "0000000000000000000000000000000000000000000000000000000000000000";
            };
          }
        '';
      };

      extraArguments = mkOption {
        description = "Extra command-line arguments to pass to earmms-keyderiv";
        type = types.listOf types.str;
        default = [];
      };
    };
  };

  config = mkIf cfg.enable {
    users.users.${cfg.user} = {
      name = cfg.user;
      group = cfg.group;
      description = pkgDesc;
      isSystemUser = true;
    };

    users.groups.${cfg.group} = {
      name = cfg.group;
    };

    systemd.services."earmms-keyderiv" = {
      description = pkgDesc;
      wantedBy = [ "default.target" ];

      environment = {
        HOST = toString cfg.listenHost;
        PORT = toString cfg.listenPort;
      };

      serviceConfig = {
        Type = "simple";
        User = cfg.user;
        Group = cfg.group;
        Restart = "always";

        ProtectSystem = "strict";
        BindReadOnlyPaths = [ keyderivConfig ];

        ExecStart = ''
          ${cfg.package}/bin/earmms-keyderiv \
            --config ${keyderivConfig} \
            ${builtins.concatStringsSep " " cfg.extraArguments}
        '';
      };
    };
  };
}
