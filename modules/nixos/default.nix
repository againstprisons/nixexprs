{
  earmms-keyderiv = ./earmms-keyderiv.nix;
  # earmms = ./earmms.nix;

  __functionArgs = { };
  __functor = self: { ... }: {
    imports = with self; [
      earmms-keyderiv
      # earmms
    ];
  };
}
