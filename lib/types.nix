{ lib, ... }:

with lib;

let
  # Keyderiv key checker
  isKeyderivKey = s: isString s && builtins.match "[a-fA-F0-9]{64}" s != null;

  # Keyderiv key type
  keyderivKey = mkOptionType {
    name = "keyderivKey";
    description = "32 bytes as a hex-encoded string (64 characters)";
    check = isKeyderivKey;
    merge = mergeEqualOption;
    emptyValue = { value = "0000000000000000000000000000000000000000000000000000000000000000"; };
  };

in {
  inherit isKeyderivKey keyderivKey;
}
