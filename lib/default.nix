{ pkgs ? import <nixpkgs> { }
, lib ? pkgs.lib
, super ? if !isOverlayLib then lib else { }
, self ? if !isOverlayLib then lib else { }
, before ? if !isOverlayLib then lib else { }
, isOverlayLib ? false
}@args:

let
  lib = before // papa // self;
  papa = with before; with papa; with self;
    {
      types = import ./types.nix { inherit lib; };
    };

in {
  inherit papa;
}
